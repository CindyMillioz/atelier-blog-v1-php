<?php

//affichage des erreurs

include dirname(__DIR__).'/src/init.php';

$page_title = 'Contactez-nous';

$page_content = '<section class="row"><div class="col-lg-6 offset-lg-3">';

$page_content = <<<FORMULAIRE

<form action="<?php dirname(__DIR__).'/validation-contact.php'?>" method="post">

    <input type="text" name="nom" placeholder="Votre nom" required class="form-control">
    <input type="text" name="prenom" placeholder="Votre prenom" required class="form-control">
    <input type="email" name="email" placeholder="Votre email" required class="form-control">
    <input type="tel" name="telephone" placeholder="Votre téléphone" required class="form-control">
    <textarea name="message" placeholder="Votre message" required class="form-control"></textarea>
    <input type="submit" value="Envoyer" class="form-control">


</form>
FORMULAIRE;

$page_content .= '</div></section>';

include dirname(__DIR__).'/src/layout.php';
