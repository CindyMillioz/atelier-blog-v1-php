<?php

//affichage des erreurs

ini_set('display_errors',1);
ini_set('log_errors',1);
error_reporting(E_ALL);

// Chargement du contenu avec Json

// 1- On définit le chemin On stock le chemin dans la variable $json_file_path

$json_file_path = dirname(__DIR__).'/src/articles.json';

// on verifie que ke fichier existe bien à cet emplacement (debug)

//var_dump(file_exists($json_file_path));
//exit();

// On vérifie que le fichier existe
if(file_exists($json_file_path) === false){

    // On lance une exception
    throw new Exception('Fichier Introuvable');
}

// On récupère le contenu du fichier json

$json_file_content = file_get_contents($json_file_path);

//var_dump($json_file_content);

// on transforme le contenu json en tableau avec json_decode
$json_data =json_decode($json_file_content,true);

// On vérifie si les données json récupérées sont valides

if(json_last_error() !==JSON_ERROR_NONE){

    throw new Exception('Error Json "%s"',json_last_error_msg());
}

// On extrait la variable articles 
$articles = $json_data['articles'];
var_dump($articles);