<?php

// 1- Configurer les messages en créant des varaibles
$message_erreur = 'Erreur les champs suivants doivent être complétés';

$message_ok = 'Votre message a bien été envoyé';
$message = $message_erreur;

// 2- Définir les constantes pour l'envoi d'emails
define('MAIL_EXPEDITEUR','cereza.jackie19@gmail.com');
define('MAIL_SUJET','Contact blog V1');


// 3- Vérifier les champs de formulaire

if(empty($_POST['nom'])) {
    $message.= ': Le champ nom est vide <br/>';
}
if(empty($_POST['prenom'])) {
    $message.= ': Le champ prénom est vide <br/>';
}

if(empty($_POST['email'])) {
    $message.= ': Le champ email est vide <br/>';
}
if(empty($_POST['telephone'])) {
    $message.= ': Le champ telephone est vide <br/>';
}

if(empty($_POST['message'])) {
    $message.= ': Le champ message est vide <br/>';
}

// 4- Vérifier que les champs soient bien remplis

// Si le message final est + long que le message d'erreur de départ =>
// erreur => affichage de l'erreur
// On enverra le mail uniquement si tous les champs sont remplis
// strlen() => string lengh => défini le nombre de caractères dans une chaine

if(strlen($message)>($message_erreur)){
    // On affiche le message d'erreur
    echo $message;

}

// sinon on créé une variable pour récupérer le contenu des champs
// en enlevant les espaces et les caractères d'échappement => trim() /stripslashes()

else {

    foreach ($_POST as $index => $valeur)
    {

        $$index= stripslashes(trim($valeur));
    }
}

// Préparation de l'entête du mail
$mail_entete="MIME-Version : 1.0\r\n";
$mail_entete.="From: {$_POST['nom']}"."<{$_POST['email']{>\r\n";
$email_entete.='Reply-To:'.$_POST['email']."\r\n";
$email_entete.='Content-Type: text/plain;charset="UTF-8"';
$email_entete.="\r\nContent-Transfer-Encoding:8bit\r\n";
$email_entete.='X-Mailer:PHP/'.phpversion()."\r\n";

// 6- Préparation du corps du mail

$mail_corps = "Message de : $nom - $prenom \r\n";
$mail_corps.="Email : $email\r\n";
$mail_corps .="Telephone : $telephone\r\n";
$mail_corps .="message : $message\r\n";

// 7- Envoi du mail

if(mail(MAIL_EXPEDITEUR,MAIL_SUJET,$mail_corps,$mail_entete)){
    // le mail est envoyé et on a message ok

    echo '<script type="text/javascript">
    alert("Votre email a bien été envoyé");
    window.location="../www/index.php";
    </script>';

}

else{
    //le mail n'a pas été envoyé

    echo 'erreur';
}